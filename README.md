# In-memory-cache
In-memory-cache provides an id based cache for application data.

## Basic usage

```javascript
const Cache = require('in-memory-cache')

const refreshAll = async () => [{
  id: 1,
  email: 'justin.trudeau@government.ca',
  created: new Date()
},
{
  id: 2,
  email: 'teresa.may@government.co.uk',
  created: new Date()
}]

const usersCache = await new Cache({ refreshAll }).refresh()

const rupaul = usersCache.get({ id: '2f6hh33' })
```
