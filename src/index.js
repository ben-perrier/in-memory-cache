const sizeOf = require('object-sizeof')

class Cache {
  /**
   * @param {function} refreshAll: Function returning the updated array of cached objects
   */
  constructor ({ refreshAll }) {
    // Set initial cache info
    this.lastRefreshTime = null
    this.refreshStatus = 'not_yet_refreshed'
    this.lastError = {}
    this.lastErrorTime = null

    this.refreshAll = refreshAll
    this.content = []
    return this
  }

  async refresh () {
    try {
      this.refreshStatus = 'in_progress'
      this.content = await this.refreshAll()
      this.refreshStatus = 'success'
      this.lastRefreshTime = new Date()
    } catch (e) {
      const { message, stack } = e
      this.refreshStatus = 'failure'
      this.lastError = { message, stack }
      this.lastErrorTime = new Date()
    }
    return this
  }

  /**
   * Returns meta data about cache execution and errors
   */
  info () {
    const { lastRefreshTime, refreshStatus, lastError, lastErrorTime } = this
    return {
      lastRefreshTime,
      refreshStatus,
      lastError,
      lastErrorTime,
      size: this.content.length,
      sizeInBytes: sizeOf(this.content)
    }
  }

  getAll () {
    return this.content
  }

  /**
   * Returns cache entries by id, ids, filterFunction or all entries if nothing is passed
   * @param {string} param0.id: 
   * @param {array} param0.ids: 
   * @param {function} param0.filter: 
   */
  get ({ id, ids = [], filterFn } = { ids: [] }) {
    if (!id && !ids.length && !filterFn) return this.getAll()
    if (filterFn) return this.content.filter(filterFn)
    ids = ids.concat(id).filter(itm => !!itm)
    return this.content.filter((itm) => ids.includes(itm.id))
  }
}

module.exports = Cache
