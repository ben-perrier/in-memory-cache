const Cache = require('../index.js')

const fn = () => [{
  id: 1,
  email: 'justin.trudeau@government.ca',
  created: new Date()
},
{
  id: 2,
  email: 'teresa.may@government.co.uk',
  created: new Date()
},
{
  id: 3,
  email: 'julia.gillard@government.com.au',
  created: new Date()
}]
const asyncFn = async () => new Promise(resolve => { resolve(fn()) })
const failureFn = async () => new Promise(() => { throw Error('Error occured') })

describe('cache.all', () => {
  describe('should return consistently the same collection', () => {
    it('with a sync function', async () => {
      const cache = await new Cache({ refreshAll: fn }).refresh()
      const all = cache.getAll()
      expect(all.length).toBe(3)
      expect(all[1].email).toBe('teresa.may@government.co.uk')
      const sameAll = await cache.getAll()
      expect(all).toEqual(sameAll)
    })

    it('with an async function', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const all = await cache.getAll()
      expect(all.length).toBe(3)
      expect(all[1].email).toBe('teresa.may@government.co.uk')

      const sameAll = await cache.getAll()
      expect(all).toEqual(sameAll)
    })
  })
})

describe('cache.refresh', () => {
  describe('it allows to refresh the collection and consistently return the same collection', () => {
    it('should work with a sync function', async () => {
      const cache = await new Cache({ refreshAll: fn }).refresh()
      const all = cache.getAll()
      const refreshed = (await cache.refresh()).getAll()
      expect(all[0].created).not.toBe(refreshed[0].created)

      const refreshedAll = cache.getAll()
      expect(refreshed).toEqual(refreshedAll)
    })

    it('should work with an async function', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const all = cache.getAll()
      const refreshed = (await cache.refresh()).getAll()
      expect(all[0].created).not.toBe(refreshed[0].created)

      const refreshedAll = cache.getAll()
      expect(refreshed).toEqual(refreshedAll)
    })
  })
})

describe('cache.info', () => {
  describe('success', () => {
    it('should capture successful refresh status and time', async () => {
      const { lastRefreshTime, refreshStatus, lastError, lastErrorTime, size, sizeInBytes } = (await new Cache({ refreshAll: asyncFn }).refresh()).info()
      expect(refreshStatus).toBe('success')
      expect(lastError).toEqual({})
      expect(lastRefreshTime instanceof Date).toBe(true)
      expect(lastErrorTime).toBe(null)
      expect(size).toBe(3)
      expect(typeof sizeInBytes).toBe('number')
    })
  })

  describe('failure', () => {
    it('should capture failure refresh status, time and error info', async () => {
      const { lastRefreshTime, refreshStatus, lastError, lastErrorTime, size, sizeInBytes } = (await new Cache({ refreshAll: failureFn }).refresh()).info()
      expect(refreshStatus).toBe('failure')
      expect(lastError.message).toEqual('Error occured')
      expect(typeof lastError.stack).toBe('string')
      expect(lastRefreshTime).toBe(null)
      expect(lastErrorTime instanceof Date).toBe(true)
      expect(size).toBe(0)
      expect(typeof sizeInBytes).toBe('number')
    })
  })

  describe('success then failure', () => {
    it('should capture success info and cache, then failure info and keep the same data in cache', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const successInfo = cache.info()
      const successContent = cache.getAll()
      expect(successInfo.refreshStatus).toBe('success')
      expect(successInfo.lastError).toEqual({})
      expect(successInfo.lastRefreshTime instanceof Date).toBe(true)
      expect(successInfo.lastErrorTime).toBe(null)
      expect(successInfo.size).toBe(3)
      expect(successInfo.sizeInBytes).not.toBe(0)
      cache.refreshAll = failureFn
      await cache.refresh()
      const failureInfo = cache.info()
      expect(failureInfo.refreshStatus).toBe('failure')
      expect(failureInfo.lastError).not.toEqual({})
      expect(failureInfo.lastRefreshTime instanceof Date).toBe(true)
      expect(failureInfo.lastErrorTime instanceof Date).toBe(true)
      expect(failureInfo.size).toBe(3)
      expect(successContent).toEqual(cache.getAll())
      expect(successInfo.sizeInBytes).not.toBe(0)
    })
  })

  describe('cache.get', () => {
    it('should return the correct cache entry by id', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const [teresa] = cache.get({ id: 2 })
      expect(teresa.id).toEqual(2)
    })

    it('should return the correct cache entries by ids', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const [justin, teresa] = cache.get({ ids: [1, 2] })
      expect(justin.id).toEqual(1)
      expect(teresa.id).toEqual(2)
    })

    it('should return the correct cache entry using filterFn', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const [teresa] = cache.get({ filterFn: ({ email }) => email === 'teresa.may@government.co.uk' })
      expect(teresa.id).toEqual(2)
    })

    it('should return all cache entries if no param is supplied', async () => {
      const cache = await new Cache({ refreshAll: asyncFn }).refresh()
      const results = cache.get()
      expect(results.length).toEqual(3)
    })
  })
})
